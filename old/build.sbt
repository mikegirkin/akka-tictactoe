name := """tictactoe"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  //ws
  "com.mohiva"    %% "play-silhouette"  % "3.0.4",
  "net.ceedubs"   %% "ficus"            % "1.1.2",


  "com.mohiva" %% "play-silhouette-testkit" % "3.0.4" % "test",
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.0-RC1" % Test
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
