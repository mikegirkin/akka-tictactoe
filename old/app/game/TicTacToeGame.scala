package gm.tictactoe.game

import java.util.UUID

class OutOfFieldException extends Throwable {}

trait GamePlayer
case object XPlayer extends GamePlayer
case object OPlayer extends GamePlayer

trait MoveAttemptResult
case object Success extends MoveAttemptResult
trait Failure extends MoveAttemptResult
case object WrongPlayer extends Failure
case object OutOfField extends Failure
case object GameFinished extends Failure
case object CellOccupied extends Failure

trait GameFieldCellState
case object Empty extends GameFieldCellState
case class Occupied(player: GamePlayer) extends GameFieldCellState

object GameFieldCellState extends GameFieldCellState {
  def empty: GameFieldCellState = Empty
  def occupied(player: GamePlayer): GameFieldCellState = Occupied(player)
}

trait GameFinishedReason
case class PlayerWon(player: GamePlayer) extends GameFinishedReason
case class PlayerQuit(player: GamePlayer) extends GameFinishedReason

//
//trait Direction
//case object Horizontal extends Direction
//case object Diagonal extends Direction
//case object Vertical extends Direction
//
//case class WinningPosition(
//  player: GamePlayer,
//  row: Int,
//  col: Int,
//  direction: Direction
//)

//case class Move(row: Int, col: Int, player: GamePlayer)

class TicTacToeGame(
  val gameId: UUID
) {

  private var _finishedReason: Option[GameFinishedReason] = None
  private val field: GameFieldState = new GameFieldState()
  private var _expectingMoveFrom: GamePlayer = XPlayer

  def makeMove(row: Int, col: Int, player: GamePlayer): MoveAttemptResult = {
      if(_finishedReason.isDefined) GameFinished
      else if (player != expectingMoveFrom) WrongPlayer
      else if (!GameFieldState.isInField(row, col)) OutOfField
      else if(field(row, col) != Empty) CellOccupied
      else {
        field.setCell(row, col, Occupied(player))
        if(field.checkWinningPosition) _finishedReason = Some(PlayerWon(player))
        else _expectingMoveFrom = nextPlayer(expectingMoveFrom)
        Success
      }
  }

  def quit(player: GamePlayer): MoveAttemptResult = {
    if(_finishedReason.isDefined) GameFinished
    else {
      _finishedReason = Some(PlayerQuit(player))
      Success
    }
  }

  def finishedReason = _finishedReason

  def expectingMoveFrom = _expectingMoveFrom

  def getCellState = field.apply _

  private def nextPlayer(player: GamePlayer) = player match {
    case XPlayer => OPlayer
    case OPlayer => XPlayer
  }
}

object TicTacToeGame {
  def createNew(): TicTacToeGame = {
    new TicTacToeGame(UUID.randomUUID())
  }
}