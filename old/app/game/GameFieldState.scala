package gm.tictactoe.game

/**
  * Created by mgirkin on 16/03/2016.
  */
object GameFieldState {
  val rowsNumber = 3
  val colsNumber = 3

  def isInField(row: Int, col: Int) = !(row < 0 || row > rowsNumber-1 || col < 0 || col > colsNumber-1)
}

class GameFieldState {
  private val fieldState =
    Array.fill(GameFieldState.rowsNumber)(Array.fill(GameFieldState.colsNumber)(GameFieldCellState.empty))

  def getCellState(row: Int, col: Int): GameFieldCellState = {
    if(!GameFieldState.isInField(row, col)) throw new OutOfFieldException()
    fieldState(row)(col)
  }

  def setCell(row: Int, col: Int, state: GameFieldCellState): Unit = {
    if(!GameFieldState.isInField(row, col)) throw new OutOfFieldException()
    fieldState(row)(col) = state
  }

  def apply(row: Int, col: Int) = getCellState(row, col)

  def checkWinningPosition: Boolean = {
    def equals(xs: Any*) =
      xs.forall(_ == Occupied(XPlayer)) || xs.forall(_ == Occupied(OPlayer))

    //hor
    val result = Seq(
      equals(this (0, 0), this (0, 1), this (0, 2)),
      equals(this (1, 0), this (1, 1), this (1, 2)),
      equals(this (2, 0), this (2, 1), this (2, 2)),
      equals(this (0, 0), this (1, 0), this (2, 0)),
      equals(this (0, 1), this (1, 1), this (2, 1)),
      equals(this (0, 2), this (1, 2), this (2, 2)),
      equals(this (0, 0), this (1, 1), this (2, 2)),
      equals(this (0, 2), this (1, 1), this (2, 0)))

    result.contains(true)
  }
}