package gm.tictactoe.game

import java.util.UUID

import akka.actor.Actor
import gm.tictactoe.game.Messages.{Quit, Move}

object Messages {

  case class Move(userId: UUID, gameId: UUID, row: Int, col: Int)

  case class Quit(userId: UUID, gameId: UUID)

}

class GameActor extends Actor {

  val game = TicTacToeGame.createNew()

  val players = Map[UUID, GamePlayer]() //TODO: How to initialize?

  private def onChange() = {

  }

  override def receive = {
    case m @ Move(userId, gameId, row, col) => {
      val result = game.makeMove(row, col, players(userId))
      sender() ! result
      if(result == Success) onChange()
    }

    case m @ Quit(userId, gameId) => {
      val result = game.quit(players(userId))
      if(result == Success) onChange()

      //Kill self
    }
  }
}
