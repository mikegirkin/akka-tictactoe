package gm.tictactoe.auth

import java.util.UUID

import com.mohiva.play.silhouette.api.Identity
import org.joda.time.DateTime

/**
  * Created by mgirkin on 05/05/2016.
  */
case class User(
  uid: UUID,
  email: String,
  passwordHash: String,
  salt: String,
  createdAt: DateTime,
  isActive: Boolean
) extends Identity

