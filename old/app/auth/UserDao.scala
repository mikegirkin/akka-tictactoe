package gm.tictactoe.auth

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.services.IdentityService

import scala.concurrent.Future

/**
  * Created by mgirkin on 05/05/2016.
  */
trait UserDao {
  def save(user: User): Future[User]
  def getByEmail(email: String): Future[Option[User]]
}

class PsqlUserDao extends IdentityService[User] {
  override def retrieve(loginInfo: LoginInfo): Future[Option[User]] = ???
}

class InmemUserDao extends UserDao {

  var storage: List[User] = List()

  override def save(user: User): Future[User] = Future {
    storage = user :: storage.filter(u => u != user)
    user
  }

  override def getByEmail(email: String): Future[Option[User]] = Future {
    storage.find(u => u.email == email)
  }
}