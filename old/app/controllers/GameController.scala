package controllers

import javax.inject._

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.stream.Materializer
import play.api.libs.json.{JsValue, Json}
import play.api.libs.streams.ActorFlow
import play.api.mvc._

@Singleton
class GameController @Inject()(
  implicit actorSystem: ActorSystem,
  materializer: Materializer,
  environment: play.api.Environment,
  configuration: play.api.Configuration
) extends Controller {

  def count = Action { Ok("It works") }

  def gameSocket = WebSocket.accept[JsValue, JsValue] { request =>
    ActorFlow.actorRef[JsValue, JsValue] { out =>
      WebSocketActor.props(out)
    }
  }

}

class WebSocketActor(out: ActorRef) extends Actor {
  def receive = {
    case msg: JsValue =>
      out ! Json.obj("status" -> "OK")
  }
}

object WebSocketActor {
  def props(out: ActorRef) = Props(new WebSocketActor(out))
}