package gm.tictactoe.test.game

import gm.tictactoe.game._
import org.scalatest.{Matchers, WordSpec}

class TicTacToeGameSpec extends WordSpec with Matchers{

  "Normal flow scenario" when {
    val game = TicTacToeGame.createNew()

    "TicTacToeGame created" should {
      "be in initial state" in {
        game.expectingMoveFrom should be(XPlayer)
      }

      "doesn't allow to make move from OPlayer" in {
        val result = game.makeMove(0, 0, OPlayer)

        result should be(WrongPlayer)
      }

      "doesn't allow an invalid move from XPlayer" in {
        val result = game.makeMove(5, 0, XPlayer)

        result should be(OutOfField)
      }

      "allow a proper move from XPlayer" in {
        val result = game.makeMove(0, 0, XPlayer)
        result should be(Success)
      }
    }

    "The game after the first valid move" should {
      "change the player it expects to make a move next" in {
        game.expectingMoveFrom === OPlayer
      }

      "return the changed state of the field" in {
        game.getCellState(0, 0) should be(Occupied(XPlayer))
      }

      "disallow XPlayer to move again" in {
        val result = game.makeMove(0, 1, XPlayer)
        result should be(WrongPlayer)
      }

      "disallow OPlayer to move into the same spot as XPlayer" in {
        val result = game.makeMove(0, 0, OPlayer)
        result should be(CellOccupied)
      }

      "allow OPlayer to make its move" in {
        val result = game.makeMove(2, 0, OPlayer)
        result should be(Success)
      }
    }

    "The game runs well till completion" should {
      "accept the moves in the order" in {
        game.makeMove(0, 1, XPlayer) should be(Success)
        game.makeMove(2, 1, OPlayer) should be(Success)
        game.makeMove(0, 2, XPlayer) should be(Success)
      }

      "correctly determine the winner of the game" in {
        game.finishedReason should be(Some(PlayerWon(XPlayer)))
      }
    }
  }

  "One player quits scenario" in {
    val game = TicTacToeGame.createNew()

    game.makeMove(0, 0, XPlayer) should be (Success)
    game.makeMove(2, 2, OPlayer) should be (Success)
    game.quit(OPlayer) should be (Success)

    game.finishedReason should be (Some(PlayerQuit(OPlayer)))
  }

}
