create table users (
  id UUID PRIMARY KEY,
  provider_id varchar(255) not null,
  provider_key varchar(255) not null,
  email VARCHAR(1023) not null UNIQUE,
  first_name varchar(255),
  last_name varchar(255),
  nickname VARCHAR(255),
  UNIQUE(provider_id, provider_key)
);

create table passwords (
  user_id UUID not null PRIMARY KEY REFERENCES users(id),
  hasher varchar(255) not null,
  password varchar(255) not NULL,
  salt VARCHAR(255)
);

create table oauth2info (
  user_id uuid not null PRIMARY KEY REFERENCES users(id),
  access_token varchar(1024) not null,
  token_type varchar(255),
  expires_in int,
  refresh_token varchar(1024),
  params varchar(1024)
);
