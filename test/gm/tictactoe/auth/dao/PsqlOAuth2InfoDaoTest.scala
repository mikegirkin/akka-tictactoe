package gm.tictactoe.auth.dao

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.impl.providers.OAuth2Info
import gm.tictactoe.auth.dao.psql.{PsqlOAuth2InfoDao, PsqlUserDao}
import gm.tictactoe.auth.dao.queries.{OAuth2InfoTableQueries, UserTableQueries}
import org.scalatest.{Matchers, WordSpec}

/**
  * Created by mgirkin on 22/05/2016.
  */
class PsqlOAuth2InfoDaoTest extends WordSpec with Matchers with DaoTestHelpers {

  import scala.concurrent.ExecutionContext.Implicits.global

  val user = randomCredentialsUser().copy(providerId = "oauth2")
  val oauth2Info = OAuth2Info("accessToken", None, None, None, Some(Map[String, String]("test" -> "passed")))
  val loginInfo = LoginInfo(user.providerId, user.providerKey)
  val userDao = new PsqlUserDao(db, new UserTableQueries())
  val oAuth2InfoDao = new PsqlOAuth2InfoDao(db, new UserTableQueries(), new OAuth2InfoTableQueries())

  "test insert" in {
    await(userDao.save(user))
    await(oAuth2InfoDao.save(loginInfo, oauth2Info))
  }

  "test retreive" in {
    val i = await{
      oAuth2InfoDao.find(loginInfo)
    }
    i should not be empty
    i.get should be (oauth2Info)
  }

  "test update" in {
    val updatedInfo = oauth2Info.copy(expiresIn = Some(10))
    val i = await {
      for {
        _ <- oAuth2InfoDao.save(loginInfo, updatedInfo)
        info <- oAuth2InfoDao.find(loginInfo)
      } yield info
    }
    i should not be empty
    i.get should be (updatedInfo)
  }

  "test remove" in {
    val p = await {
      for {
        _ <- oAuth2InfoDao.remove(loginInfo)
        info <- oAuth2InfoDao.find(loginInfo)
      } yield info
    }
    p shouldBe empty
  }

}
