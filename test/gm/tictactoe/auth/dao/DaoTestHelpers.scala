package gm.tictactoe.auth.dao

import java.util.UUID

import gm.tictactoe.auth.model.User

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import slick.driver.PostgresDriver.api._

import scala.util.Random

trait DaoTestHelpers {
  val db = Database.forConfig("testDb")

  def randomEmail() =
    s"${UUID.randomUUID().toString}@tttest.com"

  def randomString(length: Int = 15): String =
    (1 to length).map{
      _ => Random.nextPrintableChar()
    }.mkString

  def randomOpt[T](value: => T): Option[T] = {
    if(Random.nextBoolean()) Some(value)
    else None
  }

  def randomCredentialsUser() = {
    val id = UUID.randomUUID()
    val email = s"$id@ttttest.com"
    User(
      id, "credentials", email, email,
      randomOpt(randomString()), randomOpt(randomString()), randomOpt(randomString())
    )
  }

  def await[T](t: Future[T]): T = {
    Await.result(t, Duration(250, MILLISECONDS))
  }
}
