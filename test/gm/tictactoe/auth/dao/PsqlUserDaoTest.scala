package gm.tictactoe.auth.dao

import com.mohiva.play.silhouette.api.LoginInfo
import gm.tictactoe.auth.dao.psql.PsqlUserDao
import gm.tictactoe.auth.dao.queries.UserTableQueries
import org.scalatest.{Matchers, WordSpec}

class PsqlUserDaoTest extends WordSpec with Matchers with DaoTestHelpers {

  import scala.concurrent.ExecutionContext.Implicits.global

  val user = randomCredentialsUser()

  val dao = new PsqlUserDao(db, new UserTableQueries())

  "insert" in {
    await(dao.save(user))
  }

  "retreive by userId" in {
    val u = await(dao.find(user.id))
    u should not be empty
    u.get should be (user)
  }

  "retreive by loginInfo" in {
    val u = await(dao.find(LoginInfo("credentials", user.email)))
    u should not be empty
    u.get should be (user)
  }

  "update" in {
    val updatedUser = user.copy(firstName = Some("testuser"))
    val u = await {
      dao.save(updatedUser).flatMap { _ =>
        dao.find(user.id)
      }
    }
    u should not be empty
    u.get should be (updatedUser)
  }
}
