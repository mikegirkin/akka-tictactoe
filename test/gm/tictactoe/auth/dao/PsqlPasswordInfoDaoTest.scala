package gm.tictactoe.auth.dao

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import gm.tictactoe.auth.dao.psql.{PsqlPasswordInfoDao, PsqlUserDao}
import gm.tictactoe.auth.dao.queries.{PasswordInfoTableQueries, UserTableQueries}
import org.scalatest.{Matchers, WordSpec}

/**
  * Created by mgirkin on 21/05/2016.
  */
class PsqlPasswordInfoDaoTest extends WordSpec with Matchers with DaoTestHelpers {

  import scala.concurrent.ExecutionContext.Implicits.global

  val user = randomCredentialsUser()
  val loginInfo = LoginInfo("credentials", user.email)
  val pwdInfo = PasswordInfo("bcrypt", "skdjfhslkjfhsdfkj", None)
  val userDao = new PsqlUserDao(db, new UserTableQueries())
  val pwdInfoDao = new PsqlPasswordInfoDao(db, new UserTableQueries(), new PasswordInfoTableQueries())

  "insert" in {
    await(userDao.save(user))
    await(pwdInfoDao.save(loginInfo, pwdInfo))
  }

  "retreive by user" in {
    val p = await(pwdInfoDao.find(loginInfo))
    p should not be empty
    p.get should be (pwdInfo)
  }

  "update" in {
    val updatedPwd = pwdInfo.copy(password = "NewPassword")
    val p = await {
      pwdInfoDao.save(loginInfo, updatedPwd).flatMap {
        _ => pwdInfoDao.find(loginInfo)
      }
    }
    p should not be empty
    p.get should be (updatedPwd)
  }

  "delete" in {
    val p = await {
      pwdInfoDao.remove(loginInfo)
        .flatMap { _ =>
          pwdInfoDao.find(loginInfo)
        }
    }
    p shouldBe empty
  }

}
