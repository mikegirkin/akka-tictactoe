package gm.tictactoe.auth.model

import java.util.UUID

import com.mohiva.play.silhouette.api.{Identity, LoginInfo}

case class User(
  id: UUID,
  providerId: String,
  providerKey: String,
  email: String,
  firstName: Option[String],
  lastName: Option[String],
  nickname: Option[String]
) extends Identity {

  def loginInfo = LoginInfo(providerId, providerKey)

}