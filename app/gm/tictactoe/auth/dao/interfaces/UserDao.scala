package gm.tictactoe.auth.dao.interfaces

import java.util.UUID

import com.mohiva.play.silhouette.api.LoginInfo
import gm.tictactoe.auth.model.User

import scala.concurrent.Future

/**
  * Created by mgirkin on 21/05/2016.
  */
trait UserDao {
  def find(loginInfo: LoginInfo): Future[Option[User]]
  def find(userID: UUID): Future[Option[User]]
  def save(user: User): Future[User]
}
