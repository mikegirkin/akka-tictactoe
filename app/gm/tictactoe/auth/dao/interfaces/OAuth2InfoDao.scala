package gm.tictactoe.auth.dao.interfaces

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.impl.daos.DelegableAuthInfoDAO
import com.mohiva.play.silhouette.impl.providers.OAuth2Info

import scala.concurrent.Future

/**
  * Created by mgirkin on 21/05/2016.
  */
trait OAuth2InfoDao extends DelegableAuthInfoDAO[OAuth2Info] {
  def find(loginInfo: LoginInfo): Future[Option[OAuth2Info]]
  def add(loginInfo: LoginInfo, authInfo: OAuth2Info): Future[OAuth2Info]
  def update(loginInfo: LoginInfo, authInfo: OAuth2Info): Future[OAuth2Info]
  def save(loginInfo: LoginInfo, authInfo: OAuth2Info): Future[OAuth2Info]
  def remove(loginInfo: LoginInfo): Future[Unit]
}
