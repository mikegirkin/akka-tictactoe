package gm.tictactoe.auth.dao.tables

import java.util.UUID

import gm.tictactoe.auth.dao.records.DbOAuth2InfoRecord
import slick.lifted.Tag
import slick.driver.PostgresDriver.api._

class OAuth2InfoTable(tag: Tag) extends Table[DbOAuth2InfoRecord](tag, "oauth2info") {

  def userId = column[UUID]("user_id", O.PrimaryKey)
  def accessToken = column[String]("access_token")
  def tokenType = column[Option[String]]("token_type")
  def expiresIn = column[Option[Int]]("expires_in")
  def refreshToken = column[Option[String]]("refresh_token")
  def params = column[Option[String]]("params")

  override def * = (userId, accessToken, tokenType, expiresIn, refreshToken, params) <>
    ((DbOAuth2InfoRecord.apply _).tupled, DbOAuth2InfoRecord.unapply)
}
