package gm.tictactoe.auth.dao.tables

import java.util.UUID

import gm.tictactoe.auth.dao.records.DbPasswordInfo
import slick.driver.PostgresDriver.api._
import slick.lifted.Tag

class PasswordInfoTable(tag: Tag) extends Table[DbPasswordInfo](tag, "passwords") {
  def user_id = column[UUID]("user_id", O.PrimaryKey)
  def hasher = column[String]("hasher")
  def password = column[String]("password")
  def salt = column[Option[String]]("salt")

  override def * = (user_id, hasher, password, salt) <> ((DbPasswordInfo.apply _).tupled, DbPasswordInfo.unapply)
}
