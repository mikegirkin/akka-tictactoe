package gm.tictactoe.auth.dao.tables

import java.util.UUID

import gm.tictactoe.auth.model.User
import slick.lifted.Tag
import slick.driver.PostgresDriver.api._

class UserTable(tag: Tag) extends Table[User](tag, "users") {

  def id = column[UUID]("id", O.PrimaryKey)
  def providerId = column[String]("provider_id")
  def providerKey = column[String]("provider_key")
  def email = column[String]("email")
  def first_name = column[Option[String]]("first_name")
  def last_name = column[Option[String]]("last_name")
  def nickname = column[Option[String]]("nickname")

  override def * = (id, providerId, providerKey, email, first_name, last_name, nickname) <> (User.tupled, User.unapply)

}
