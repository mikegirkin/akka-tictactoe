package gm.tictactoe.auth.dao.psql

import java.util.UUID
import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import gm.tictactoe.auth.dao.interfaces.UserDao
import gm.tictactoe.auth.dao.queries.UserTableQueries
import gm.tictactoe.auth.model.User
import slick.driver.PostgresDriver.api._

class PsqlUserDao @Inject()(
  db: Database,
  users: UserTableQueries) extends UserDao {

  def find(loginInfo: LoginInfo) = {
    db.run {
      users.getByLoginInfo(loginInfo.providerID, loginInfo.providerKey)
    }
  }

  def find(userID: UUID) = {
    db.run {
      users.getById(userID)
    }
  }

  def save(user: User) = {
    db.run {
      users.save(user)
    }
  }
}