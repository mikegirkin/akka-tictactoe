package gm.tictactoe.auth.dao.psql

import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.impl.providers.OAuth2Info
import gm.tictactoe.auth.dao.interfaces.OAuth2InfoDao
import gm.tictactoe.auth.dao.queries.{OAuth2InfoTableQueries, UserTableQueries}
import gm.tictactoe.auth.dao.records.DbOAuth2InfoRecord
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.PostgresDriver.api._

import scala.concurrent.Future

class PsqlOAuth2InfoDao @Inject()(
  db: Database,
  userTableQueries: UserTableQueries,
  oauthInfoTableQueries: OAuth2InfoTableQueries
) extends OAuth2InfoDao {

  private def getInfoByLogin(loginInfo: LoginInfo) = {
    for {
      (user, info) <- userTableQueries.users join oauthInfoTableQueries.table on (_.id === _.userId)
        if user.providerId === loginInfo.providerID && user.providerKey === loginInfo.providerKey
    } yield info
  }

  def find(loginInfo: LoginInfo): Future[Option[OAuth2Info]] =
    db.run {
      getInfoByLogin(loginInfo)
      .result.headOption
      .map {
        _.map(_.toOAuth2Info)
      }
    }

  def add(loginInfo: LoginInfo, authInfo: OAuth2Info): Future[OAuth2Info] = save(loginInfo, authInfo)

  def update(loginInfo: LoginInfo, authInfo: OAuth2Info): Future[OAuth2Info] = save(loginInfo, authInfo)

  def save(loginInfo: LoginInfo, authInfo: OAuth2Info): Future[OAuth2Info] =
    db.run {
      val user = userTableQueries.getByLoginInfo(loginInfo)
      user.flatMap {
        case None => DBIO.failed(new NoSuchElementException)
        case Some(u) => oauthInfoTableQueries.upsert(DbOAuth2InfoRecord.fromOAuth2Info(u.id, authInfo))
      }
    }.map {
      _.toOAuth2Info
    }

  def remove(loginInfo: LoginInfo): Future[Unit] =
    db.run {
      val info = getInfoByLogin(loginInfo)
      oauthInfoTableQueries.table.filter { i =>
        i.userId in info.map{_.userId}
      }.delete
    }.map { _ => Unit}
}