package gm.tictactoe.auth.dao.psql

import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.impl.daos.DelegableAuthInfoDAO
import gm.tictactoe.auth.dao.queries.{PasswordInfoTableQueries, UserTableQueries}
import slick.driver.PostgresDriver.api._

import scala.concurrent.Future

class PsqlPasswordInfoDao @Inject()(
  db: Database,
  users: UserTableQueries,
  passwordInfo: PasswordInfoTableQueries) extends DelegableAuthInfoDAO[PasswordInfo] {

  import play.api.libs.concurrent.Execution.Implicits.defaultContext

  def find(loginInfo: LoginInfo): Future[Option[PasswordInfo]] = {
    db.run {
      (for {
        user <- users.users if user.providerId === loginInfo.providerID && user.providerKey === loginInfo.providerKey
        pwd <- passwordInfo.passwords if user.id === pwd.user_id
      } yield pwd)
      .result.headOption
    }.map {
      _.map { _.toPasswordInfo }
    }
  }

  def add(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] =
    save(loginInfo, authInfo)

  def update(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] =
    save(loginInfo, authInfo)

  def save(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = {
    db.run {
      val user = users.getByLoginInfo(loginInfo.providerID, loginInfo.providerKey)
      user.flatMap {
        case None => DBIO.failed(new NoSuchElementException())
        case Some(u) => passwordInfo.upsert(u.id, authInfo)
      }
    }.map(_.toPasswordInfo)
  }

  def remove(loginInfo: LoginInfo): Future[Unit] = {
    db.run {
      val user = users.users
        .filter{ u => u.providerId === loginInfo.providerID && u.providerKey === loginInfo.providerKey}
      passwordInfo.passwords.filter(x => x.user_id in user.map(_.id))
        .delete
    }.map { _ => Unit }
  }
}
