package gm.tictactoe.auth.dao.records

import java.util.UUID

import com.mohiva.play.silhouette.impl.providers.OAuth2Info
import play.api.libs.json.{JsObject, Json}

case class DbOAuth2InfoRecord(
  userId: UUID,
  accessToken: String,
  tokenType: Option[String],
  expiresIn: Option[Int],
  refreshToken: Option[String],
  params: Option[String]
) {
  def toOAuth2Info: OAuth2Info = {
    OAuth2Info(accessToken, tokenType, expiresIn, refreshToken, params.map(DbOAuth2InfoRecord.deserializeParams))
  }
}

object DbOAuth2InfoRecord {
  def deserializeParams(params: String): Map[String, String] = {
    Json.parse(params).as[JsObject].fields.map {
      case (key, value) => key -> value.as[String]
    }.toMap
  }

  def serializeParams(params: Map[String, String]): String = {
    Json.toJson(params).toString()
  }

  def fromOAuth2Info(userId: UUID, info: OAuth2Info) = {
    DbOAuth2InfoRecord(userId, info.accessToken, info.tokenType, info.expiresIn, info.refreshToken, info.params.map(serializeParams))
  }
}
