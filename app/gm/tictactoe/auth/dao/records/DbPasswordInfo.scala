package gm.tictactoe.auth.dao.records

import java.util.UUID

import com.mohiva.play.silhouette.api.util.PasswordInfo

case class DbPasswordInfo(userId: UUID, hasher: String, password: String, salt: Option[String]) {
  def toPasswordInfo = PasswordInfo(hasher, password, salt)
}

object DbPasswordInfo {
  def fromPasswordInfo(userId: UUID, pwdInfo: PasswordInfo) =
    DbPasswordInfo(userId, pwdInfo.hasher, pwdInfo.password, pwdInfo.salt)
}
