package gm.tictactoe.auth.dao.queries

import java.util.UUID

import com.mohiva.play.silhouette.api.LoginInfo
import gm.tictactoe.auth.dao.tables.UserTable
import gm.tictactoe.auth.model.User
import slick.driver.PostgresDriver.api._

class UserTableQueries {

  val users = TableQuery[UserTable]

  import play.api.libs.concurrent.Execution.Implicits.defaultContext

  def getById(id: UUID): DBIO[Option[User]] =
    users.filter { x => x.id === id }
      .take(1)
      .result.headOption

  def getByLoginInfo(providerId: String, providerKey: String): DBIO[Option[User]] =
    users.filter { x => x.providerId === providerId && x.providerKey === providerKey }
      .take(1)
      .result.headOption

  def getByLoginInfo(loginInfo: LoginInfo): DBIO[Option[User]] =
    getByLoginInfo(loginInfo.providerID, loginInfo.providerKey)

  def save(user: User): DBIO[User] =
    (users returning users).insertOrUpdate(user).map {
      case None => user
      case Some(u) => u
    }
}
