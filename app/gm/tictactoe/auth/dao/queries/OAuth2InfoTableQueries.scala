package gm.tictactoe.auth.dao.queries

import gm.tictactoe.auth.dao.records.DbOAuth2InfoRecord
import gm.tictactoe.auth.dao.tables.OAuth2InfoTable
import slick.lifted.TableQuery
import slick.driver.PostgresDriver.api._

class OAuth2InfoTableQueries {

  import play.api.libs.concurrent.Execution.Implicits.defaultContext

  val table = TableQuery[OAuth2InfoTable]

  def upsert(info: DbOAuth2InfoRecord) = {
    (table returning table).insertOrUpdate(info).map {
      case None => info
      case Some(u) => u
    }
  }
}
