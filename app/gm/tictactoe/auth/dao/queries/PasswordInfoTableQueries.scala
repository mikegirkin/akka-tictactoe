package gm.tictactoe.auth.dao.queries

import java.util.UUID

import com.mohiva.play.silhouette.api.util.PasswordInfo
import gm.tictactoe.auth.dao.records.DbPasswordInfo
import gm.tictactoe.auth.dao.tables.PasswordInfoTable
import slick.driver.PostgresDriver.api._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

class PasswordInfoTableQueries {

  val passwords = TableQuery[PasswordInfoTable]

  def getByUserId(userId: UUID) = {
    passwords.filter(p => p.user_id === userId)
      .take(1)
      .result.headOption
  }

  def upsert(userId: UUID, pwdInfo: PasswordInfo) = {
    val passwordInfo = DbPasswordInfo.fromPasswordInfo(userId, pwdInfo)
    (passwords returning passwords).insertOrUpdate(passwordInfo).map {
      case None => passwordInfo
      case Some(u) => u
    }
  }
}