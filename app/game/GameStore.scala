package gm.tictactoe.game

import java.util.UUID

import scala.concurrent.Future

/**
  * Created by mgirkin on 17/03/2016.
  */
trait GameStore {
  def save(ticTacToeGame: TicTacToeGame): Future[Unit]
  def load(gameId: UUID): Future[Option[TicTacToeGame]]
}

object InMemoryGameStore extends GameStore {
  val games = scala.collection.mutable.HashMap[UUID, TicTacToeGame]()

  override def save(ticTacToeGame: TicTacToeGame): Future[Unit] = {
    games.update(ticTacToeGame.gameId, ticTacToeGame)
    Future.successful(Unit)
  }

  override def load(gameId: UUID): Future[Option[TicTacToeGame]] = {
    Future.successful(games.get(gameId))
  }
}


